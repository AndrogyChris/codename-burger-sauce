using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.Events;
using FMODUnity;

public class ShearsController : MonoBehaviour
{
    public static UnityEvent OnPlayerDeath = new UnityEvent();
    public static UnityEvent OnPlayerReset = new UnityEvent();
    public static UnityEvent OnPlayStart = new UnityEvent();

    [SerializeField] float playerSpeed;
    [SerializeField] float rotateSpeed;

    [SerializeField] CinemachineVirtualCamera virtualCam;

    [SerializeField] float startingScale;
    [SerializeField] float startingCamDistance;

    [SerializeField] GameObject visuals;
    [SerializeField] GameObject objectEater;

    [SerializeField] EventReference audio_SnipEvent;
    FMOD.Studio.EventInstance audio_SnipInstance;

    int currentVirtualCamScaleIndex;

    float currentScale;

    bool waitingForInput;
    bool dying;

    Vector3 moveInput;

    Rigidbody rb;
    Animator anim;

    bool isClosing;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        waitingForInput = true;
    }

    private void Start()
    {
        SetupSFX();
        ResetShears();
    }

    private void OnEnable()
    {
        InputManager.OnStickSteerInput.AddListener(DetermineMoveDirection);
        InputManager.OnTapInput.AddListener(CloseShears);

        ConsumableBase.OnObjectConsumed.AddListener(IncreaseSize);

        TimeManager.OnTimerCompleted.AddListener(PlayerDeath);
    }

    private void OnDisable()
    {
        InputManager.OnStickSteerInput.RemoveListener(DetermineMoveDirection);
        InputManager.OnTapInput.RemoveListener(CloseShears);

        ConsumableBase.OnObjectConsumed.RemoveListener(IncreaseSize);

        TimeManager.OnTimerCompleted.RemoveListener(PlayerDeath);
    }

    private void FixedUpdate()
    {
        Move();
        RotateInDirectionOfVelocity();
    }

    private void DetermineMoveDirection(Vector2 incommingInput)
    {
        if (!dying)
        {
            if (waitingForInput)
            {
                waitingForInput = false;
                OnPlayStart.Invoke();
            }
            moveInput = new Vector3(incommingInput.x, 0, incommingInput.y).normalized;
        }
    }

    private void Move()
    {
        if (!waitingForInput)
            rb.velocity = new Vector3(moveInput.x, 0, moveInput.z) * playerSpeed * Time.fixedDeltaTime;
        else
            rb.velocity = Vector3.zero;
    }

    private void RotateInDirectionOfVelocity()
    {
        float targetAngle = Mathf.Atan2(moveInput.x, moveInput.z) * Mathf.Rad2Deg;
        rb.MoveRotation(Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0f, targetAngle, 0f), rotateSpeed));
    }

    [ContextMenu("Reset Player")]
    public void ResetShears() //Called from UI
    {
        transform.localPosition = Vector3.zero;
        currentScale = startingScale;
        transform.localScale = startingScale * Vector3.one;
        transform.localEulerAngles = Vector3.zero;
        rb.velocity = Vector3.zero;
        if (virtualCam != null) virtualCam.GetCinemachineComponent<CinemachineFramingTransposer>().m_CameraDistance = startingCamDistance;
        currentVirtualCamScaleIndex = 1;

        visuals.SetActive(true);
        objectEater.SetActive(true);

        dying = false;
        OnPlayerReset.Invoke();
    }

    private void CloseShears()
    {
        if (!isClosing)
        {
            isClosing = true;
            anim.SetTrigger("closeShears");
            PlaySFX();
        }
    }

    public void OpenShears() //Anim event
    {
        isClosing = false;
    }

    private void SetupSFX()
    {
        audio_SnipInstance = RuntimeManager.CreateInstance(audio_SnipEvent);
        RuntimeManager.AttachInstanceToGameObject(audio_SnipInstance, transform);

    }

    private void PlaySFX()
    {
        audio_SnipInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        audio_SnipInstance.start();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.TryGetComponent<ConsumableBase>(out ConsumableBase consumableBase))
        {
            if (currentScale/3 > consumableBase.consumeStrength)
            {
                consumableBase.OnConsumed();
            }
            else
            {
                PlayerDeath();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent<ConsumableBase>(out ConsumableBase consumableBase))
        {
            if (currentScale >= consumableBase.consumeStrength)
            {
                consumableBase.OnConsumed();
            }
            else
            {
                PlayerDeath();
            }
        }
    }

    private void IncreaseSize(int consumedSize)
    {
        currentScale += (float)consumedSize / 100;
        transform.localScale = Vector3.one * currentScale;

        if (currentScale > currentVirtualCamScaleIndex)
        {
            currentVirtualCamScaleIndex++;
            virtualCam.GetCinemachineComponent<CinemachineFramingTransposer>().m_CameraDistance++;
        }
    }

    private void PlayerDeath()
    {
        dying = true;
        waitingForInput = true;
        visuals.SetActive(false);
        objectEater.SetActive(false);
        OnPlayerDeath.Invoke();
    }
}
