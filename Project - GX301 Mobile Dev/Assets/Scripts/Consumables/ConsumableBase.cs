using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using FMODUnity;

public class ConsumableBase : MonoBehaviour
{
    public static UnityEvent<int> OnObjectConsumed = new UnityEvent<int>();

    //public ConsumableSO consumableSO;

    public float size;

    public float consumeStrength;

    CapsuleCollider objectCollider;

    GameObject vis;

    bool isSetUp;

    [SerializeField] EventReference audio_chompEvent;
    FMOD.Studio.EventInstance audio_chompInstance;

    private void Awake()
    {
        objectCollider = GetComponent<CapsuleCollider>();
    }

    private void Start()
    {
        SetUpSFX();
    }

    [ContextMenu("Spawn Consumable")]
    public void SetUpConsumable(ConsumableSO consumableSO)
    {
        if (!isSetUp)
        {
            name = consumableSO.consumableName;
            objectCollider.radius = consumableSO.consumableSize / 2;
            transform.localScale = Vector3.one * consumableSO.consumableSizeMultiplier;
            consumeStrength = consumableSO.consumableSize * consumableSO.consumableStrength * consumableSO.consumableSizeMultiplier;
            vis = Instantiate(consumableSO.consumableVisuals, transform);
            vis.transform.localPosition = Vector3.zero;
            isSetUp = true;
        }
    }

    public void OnConsumed()
    {
        PlaySFX();
        OnObjectConsumed.Invoke((int)(consumeStrength * 10));
        ConsumableSpawner.Instance.DespawnConsumable(this);
        ObjectPoolConsumables.Instance.ReturnToObjectPool(this);
    }

    private void SetUpSFX()
    {
        audio_chompInstance = RuntimeManager.CreateInstance(audio_chompEvent);
        RuntimeManager.AttachInstanceToGameObject(audio_chompInstance, transform);
    }

    private void PlaySFX()
    {
        audio_chompInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        audio_chompInstance.start();
    }
}
