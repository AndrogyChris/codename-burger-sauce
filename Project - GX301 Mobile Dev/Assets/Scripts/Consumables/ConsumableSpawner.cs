using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsumableSpawner : MonoBehaviour
{
    public static ConsumableSpawner Instance;

    [SerializeField] Vector2 spawnArea;
    [SerializeField] Vector2 spawnDeadzone;

    [SerializeField] List<ConsumableSpawns> consumableSpawns = new List<ConsumableSpawns>();

    List<ConsumableBase> sceneConsumables = new List<ConsumableBase>();

    bool shouldRespawnObjects;

    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        ShearsController.OnPlayerDeath.AddListener(DespawnAllConsumables);
        ShearsController.OnPlayerReset.AddListener(SpawnObjects);
    }

    private void OnDisable()
    {
        ShearsController.OnPlayerDeath.RemoveListener(DespawnAllConsumables);
        ShearsController.OnPlayerReset.RemoveListener(SpawnObjects);
    }

    [ContextMenu("Spawn Consumables")]
    private void SpawnObjects()
    {
        DespawnAllConsumables();

        foreach (ConsumableSpawns spawns in consumableSpawns)
        {
            for (int i = 0; i < spawns.maxNumberOfConsumable; i++)
            {
                SpawnConsumable(spawns);
            }
        }

        shouldRespawnObjects = true;

        StartCoroutine(RespawnSomeObjects());
    }

    IEnumerator RespawnSomeObjects()
    {
        yield return new WaitForSeconds(5);

        if (shouldRespawnObjects)
        {
            for (int i = 0; i < ObjectPoolConsumables.Instance.pool.Count; i++)
            {
                ConsumableBase consumable = GetConsumable();
                Vector3 spawnPos = RandomSpawnLocation();
                consumable.transform.localPosition = spawnPos;
            }

            StartCoroutine(RespawnSomeObjects());
        }
    }

    private void SpawnConsumable(ConsumableSpawns spawns)
    {
        ConsumableBase consumable = GetConsumable();
        consumable.SetUpConsumable(spawns.consumableToSpawn);
        Vector3 spawnPos = RandomSpawnLocation();
        consumable.transform.localPosition = spawnPos;
    }

    private Vector3 RandomSpawnLocation()
    {
        Vector3 spawnLocation = new Vector3(Random.Range(-spawnArea.x, spawnArea.x), 0, Random.Range(-spawnArea.y, spawnArea.y));
        if (spawnLocation.x > -spawnDeadzone.x && spawnLocation.x < spawnDeadzone.x && spawnLocation.z > -spawnDeadzone.y && spawnLocation.z < spawnDeadzone.y)
        {
            spawnLocation = RandomSpawnLocation();
        }
        return spawnLocation;
    }

    private ConsumableBase GetConsumable()
    {
        ConsumableBase cons = null;
        cons = ObjectPoolConsumables.Instance.GetFromObjectPool();
        cons.transform.SetParent(transform);
        sceneConsumables.Add(cons);
        //Choose Size
        return cons;
    }

    public void DespawnConsumable(ConsumableBase con)
    {
        sceneConsumables.Remove(con);
    }

    private void DespawnAllConsumables()
    {
        shouldRespawnObjects = false;

        for (int i = sceneConsumables.Count - 1; i >= 0; i--)
        {
            ObjectPoolConsumables.Instance.ReturnToObjectPool(sceneConsumables[i]);
        }
        sceneConsumables.Clear();
    }
}

[System.Serializable]
public class ConsumableSpawns
{
    public ConsumableSO consumableToSpawn;
    public int maxNumberOfConsumable;
}
