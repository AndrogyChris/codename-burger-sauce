using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Consumable Object", menuName = "Consumable Data", order = 0)]
public class ConsumableSO : ScriptableObject
{
    public string consumableName;

    public float consumableSize;
    public float consumableStrength;

    public float consumableSizeMultiplier;

    public GameObject consumableVisuals;

    public int consumablePoints;
}
