using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool<T> : MonoBehaviour where T : MonoBehaviour
{
    public static ObjectPool<T> Instance;

    [SerializeField] GameObject opPrefab;
    public Queue<T> pool = new Queue<T>();

    private void Awake()
    {
        Instance = this;
    }

    public T GetFromObjectPool()
    {
        T opObject = default(T);

        if (pool.Count > 0)
        {
            opObject = pool.Dequeue();
        }
        else
        {
            opObject = Instantiate(opPrefab, transform).GetComponent<T>();
        }

        opObject.gameObject.SetActive(true);
        return opObject;
    }

    public void ReturnToObjectPool(T opObject)
    {
        pool.Enqueue(opObject);
        opObject.gameObject.SetActive(false);
        opObject.transform.SetParent(transform);
    }
}
