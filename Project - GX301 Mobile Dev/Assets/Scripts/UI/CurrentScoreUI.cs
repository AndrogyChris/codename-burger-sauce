using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class CurrentScoreUI : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI scoreText;
    [SerializeField] TextMeshProUGUI deathScoreText;

    private void OnEnable()
    {
        ScoreManager.OnScoreChange.AddListener(UpdateScoreboard);
    }

    private void OnDisable()
    {
        ScoreManager.OnScoreChange.RemoveListener(UpdateScoreboard);
    }

    private void UpdateScoreboard(int value)
    {
        scoreText.text = value.ToString();
        deathScoreText.text = $"FINAL SCORE:\n{value}";
    }
}
