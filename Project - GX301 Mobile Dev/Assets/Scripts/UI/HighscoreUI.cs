using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab.ClientModels;
using TMPro;

public class HighscoreUI : MonoBehaviour
{
    [SerializeField] ObjectPoolLeaderboardEntryUI opEntryUI;
    List<LeaderboardEntryUI> entries = new List<LeaderboardEntryUI>();

    private void OnEnable()
    {
        UserProfile.OnLeaderboardHighScoreUpdated.AddListener(LeaderboardHighscoreUpdated);
    }
    private void OnDisable()
    {
        UserProfile.OnLeaderboardHighScoreUpdated.RemoveListener(LeaderboardHighscoreUpdated);
    }

    private void LeaderboardHighscoreUpdated(List<PlayerLeaderboardEntry> leaderboard)
    {
        if (entries.Count > 0)
        {
            for (int i = entries.Count - 1; i >= 0; i--)
            {
                opEntryUI.ReturnToObjectPool(entries[i]);
            }
        }
        entries.Clear();
        for (int i = 0; i < leaderboard.Count; i++)
        {
            LeaderboardEntryUI entry = opEntryUI.GetFromObjectPool();
            entry.SetLeaderboardEntry(leaderboard[i]);
            entries.Add(entry);
            entry.transform.SetAsLastSibling();

            if (leaderboard[i].PlayFabId == PlayfabAccountManager.playfabID)
            {
                if (leaderboard[i].StatValue != UserProfile.Instance.highscore)
                {
                    PlayfabAccountManager.Instance.GetLeaderboardDelayed("High Score");
                }
                    
            }
        }
    }
}
