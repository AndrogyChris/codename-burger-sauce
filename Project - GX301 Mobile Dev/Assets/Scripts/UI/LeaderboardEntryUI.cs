using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LeaderboardEntryUI : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI leaderboardNameText;
    [SerializeField] TextMeshProUGUI leaderboardScoreText;

    public void SetLeaderboardEntry(PlayFab.ClientModels.PlayerLeaderboardEntry entry)
    {
        leaderboardNameText.text = $"{(entry.Position + 1)}. {entry.DisplayName}";
        if (entry.PlayFabId == PlayfabAccountManager.playfabID)
            leaderboardScoreText.text = UserProfile.Instance.highscore.ToString();
        else
            leaderboardScoreText.text = entry.StatValue.ToString();
    }
}
