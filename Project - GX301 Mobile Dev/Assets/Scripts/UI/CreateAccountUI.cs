using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CreateAccountUI : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI errorText;

    [SerializeField] Canvas thisCanvas, playGameCanvas;

    string username, password, emailAddress;

    private void OnEnable()
    {
        PlayfabAccountManager.OnSignInFail.AddListener(FailedAccountCreate);

        PlayfabAccountManager.OnSignInSuccess.AddListener(SignInSuccess);
    }

    private void OnDisable()
    {
        PlayfabAccountManager.OnSignInFail.RemoveListener(FailedAccountCreate);

        PlayfabAccountManager.OnSignInSuccess.RemoveListener(SignInSuccess);
    }

    private void FailedAccountCreate(string error)
    {
        errorText.text = error;
    }

    private void SignInSuccess()
    {
        thisCanvas.enabled = false;
        playGameCanvas.enabled = true;
    }

    public void UpdateUsername (string _username)
    {
        username = _username;
    }
    public void UpdatePassword(string _password)
    {
        password = _password;
    }
    public void UpdateEmail(string _email)
    {
        emailAddress = _email;
    }

    public void CreateAccount()
    {
        PlayfabAccountManager.Instance.CreateAccount(emailAddress, username, password);
    }
}
