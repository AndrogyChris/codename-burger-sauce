using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class SignInAccountUI : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI errorText;
    [SerializeField] Canvas thisCanvas, playGameCanvas;

    string username, password;

    private void OnEnable()
    {
        PlayfabAccountManager.OnSignInFail.AddListener(FailedSignIn);
        PlayfabAccountManager.OnSignInSuccess.AddListener(SignInSuccess);
    }

    private void OnDisable()
    {
        PlayfabAccountManager.OnSignInFail.RemoveListener(FailedSignIn);
        PlayfabAccountManager.OnSignInSuccess.RemoveListener(SignInSuccess);
    }

    private void FailedSignIn(string error)
    {
        errorText.text = error;
    }

    private void SignInSuccess()
    {
        thisCanvas.enabled = false;
        playGameCanvas.enabled = true;
    }

    public void UpdateUsername(string _username)
    {
        username = _username;
    }
    public void UpdatePassword(string _password)
    {
        password = _password;
    }

    public void SignIn()
    {
        PlayfabAccountManager.Instance.SignIn(username, password);
    }
}
