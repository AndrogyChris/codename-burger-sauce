using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TimerUI : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI timerText;
    [SerializeField] TextMeshProUGUI largetext;

    private void OnEnable()
    {
        TimeManager.OnTimeChanged.AddListener(UpdateTimerUI);
    }

    private void OnDisable()
    {
        TimeManager.OnTimeChanged.RemoveListener(UpdateTimerUI);
    }

    private void UpdateTimerUI(int time)
    {
        largetext.enabled = false;
        timerText.enabled = true;

        int minutes = time / 60;
        int seconds = time % 60;

        timerText.text = $"{minutes.ToString("00")}:{seconds.ToString("00")}";

        if (time <= 5)
        {
            largetext.enabled = true;
            timerText.enabled = false;
            largetext.text = seconds.ToString();
        }
    }
}
