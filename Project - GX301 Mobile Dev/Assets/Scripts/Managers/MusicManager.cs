using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    private void OnEnable()
    {
        SceneLoader.OnStartScreenLoaded.AddListener(DestroyMusicManager);
    }

    private void OnDisable()
    {
        SceneLoader.OnStartScreenLoaded.RemoveListener(DestroyMusicManager);
    }

    private void DestroyMusicManager()
    {
        Destroy(this.gameObject);
    }
}
