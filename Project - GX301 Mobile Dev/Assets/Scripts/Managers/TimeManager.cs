using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TimeManager : MonoBehaviour
{
    [SerializeField] int startTime;

    int currentTime;

    public static UnityEvent<int> OnTimeChanged = new UnityEvent<int>();
    public static UnityEvent OnTimerCompleted = new UnityEvent();

    Coroutine reduceTimeCoroutine;

    private void OnEnable()
    {
        ShearsController.OnPlayerReset.AddListener(SetUpTimer);
        ShearsController.OnPlayStart.AddListener(StartTimer);
        ShearsController.OnPlayerDeath.AddListener(StopTimer);
    }

    private void OnDisable()
    {
        ShearsController.OnPlayerReset.RemoveListener(SetUpTimer);
        ShearsController.OnPlayStart.RemoveListener(StartTimer);
        ShearsController.OnPlayerDeath.RemoveListener(StopTimer);
    }

    private void Start()
    {
        SetUpTimer();
    }

    [ContextMenu("Set Timer")]
    private void SetUpTimer()
    {
        currentTime = startTime;
        OnTimeChanged.Invoke(currentTime);
    }

    private void StartTimer()
    {
        reduceTimeCoroutine = StartCoroutine(ReduceTimer());
        Debug.Log("Timer started");
    }

    private void StopTimer()
    {
        StopCoroutine(reduceTimeCoroutine);
        Debug.Log("Timer Stopped");
    }

    IEnumerator ReduceTimer()
    {
        yield return new WaitForSeconds(1f);

        currentTime--;
        OnTimeChanged.Invoke(currentTime);

        if (currentTime > 0)
        {
            reduceTimeCoroutine = StartCoroutine(ReduceTimer());
        }
        else
        {
            OnTimerCompleted.Invoke();
        }
    }
}
