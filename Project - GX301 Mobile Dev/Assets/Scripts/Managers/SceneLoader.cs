using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class SceneLoader : MonoBehaviour
{
    public static UnityEvent OnStartScreenLoaded = new UnityEvent();

    public void LoadStartMenu()
    {
        OnStartScreenLoaded.Invoke();
        SceneManager.LoadScene(0);
    }

    public void LoadGameScene()
    {
        SceneManager.LoadScene(1);
    }
}
