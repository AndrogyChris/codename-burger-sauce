using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ScoreManager : MonoBehaviour
{
    int currentScore;

    public static UnityEvent<int> OnScoreChange = new UnityEvent<int>();
    public static UnityEvent<int> OnFinaliseScore = new UnityEvent<int>();

    private void Start()
    {
        IncreaseScore(0);
    }

    private void OnEnable()
    {
        ConsumableBase.OnObjectConsumed.AddListener(IncreaseScore);
        ShearsController.OnPlayerDeath.AddListener(FinaliseScore);
        ShearsController.OnPlayerReset.AddListener(ResetScore);
    }

    private void OnDisable()
    {
        ConsumableBase.OnObjectConsumed.RemoveListener(IncreaseScore);
        ShearsController.OnPlayerDeath.RemoveListener(FinaliseScore);
        ShearsController.OnPlayerReset.RemoveListener(ResetScore);
    }

    private void IncreaseScore(int score)
    {
        currentScore += score;
        OnScoreChange.Invoke(currentScore);
    }

    private void FinaliseScore()
    {
        OnFinaliseScore.Invoke(currentScore);
    }

    private void ResetScore()
    {
        IncreaseScore(-currentScore);
    }
}
