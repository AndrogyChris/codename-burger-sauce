using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class InputManager : MonoBehaviour
{
    public static InputManager Instance;

    public static UnityEvent<Vector2> OnGyroSteerInput = new UnityEvent<Vector2>();
    public static UnityEvent<Vector2> OnStickSteerInput = new UnityEvent<Vector2>();
    public static UnityEvent OnTapInput = new UnityEvent();

    PlayerActions actions;

    private void Awake()
    {
        Instance = this;

        actions = new PlayerActions();
        actions.Enable();
    }

    private void OnDestroy()
    {
        actions.Disable();
    }

    private void Start()
    {
        if (UnityEngine.InputSystem.Gyroscope.current != null)
        {
            InputSystem.EnableDevice(UnityEngine.InputSystem.Gyroscope.current);
            actions.playerControls.gyro.performed += GyroSteerInput;
        }

        actions.playerControls.joystickSteering.performed += StickSteerInput;
        //actions.playerControls.joystickSteering.canceled += StickSteerInput;

        actions.playerControls.tap.performed += x => OnTapInput.Invoke();
    }

    private void GyroSteerInput(InputAction.CallbackContext ctx)
    {
        Vector2 input = ctx.ReadValue<Vector3>();
        OnGyroSteerInput.Invoke(input);
    }

    private void StickSteerInput(InputAction.CallbackContext ctx)
    {
        Vector2 input = ctx.ReadValue<Vector2>();
        OnStickSteerInput.Invoke(input);
    }
}
