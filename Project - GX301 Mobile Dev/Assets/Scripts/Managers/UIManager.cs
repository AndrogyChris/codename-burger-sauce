using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    [SerializeField] Canvas scoreUI;
    [SerializeField] Canvas inputsUI;
    [SerializeField] Canvas deathUI;
    [SerializeField] Canvas tutorialUI;
    [SerializeField] Canvas timerUI;

    private void Start()
    {
        DeactivateSpecificUI(deathUI);
        DeactivateSpecificUI(scoreUI);
        ActivateSpecificUI(inputsUI);
        ActivateSpecificUI(tutorialUI);
    }

    private void OnEnable()
    {
        ShearsController.OnPlayerDeath.AddListener(OnPlayerDeathUI);
        ShearsController.OnPlayerReset.AddListener(OnResetUI);
        ShearsController.OnPlayStart.AddListener(OnGameplayStartedUI);
    }

    private void OnDisable()
    {
        ShearsController.OnPlayerDeath.RemoveListener(OnPlayerDeathUI);
        ShearsController.OnPlayerReset.RemoveListener(OnResetUI);
        ShearsController.OnPlayStart.RemoveListener(OnGameplayStartedUI);
    }

    private void OnGameplayStartedUI()
    {
        DeactivateSpecificUI(tutorialUI);
        ActivateSpecificUI(scoreUI);
    }

    private void OnPlayerDeathUI()
    {
        DeactivateSpecificUI(scoreUI);
        DeactivateSpecificUI(inputsUI);
        DeactivateSpecificUI(timerUI);
        ActivateSpecificUI(deathUI);
    }

    private void OnResetUI()
    {
        DeactivateSpecificUI(deathUI);
        ActivateSpecificUI(inputsUI);
        ActivateSpecificUI(tutorialUI);
        ActivateSpecificUI(timerUI);
    }

    private void DeactivateSpecificUI(Canvas uiToDeactivate)
    {
        uiToDeactivate.enabled = false;
    }

    private void ActivateSpecificUI(Canvas uiToActivate)
    {
        uiToActivate.enabled = true;
    }
}
