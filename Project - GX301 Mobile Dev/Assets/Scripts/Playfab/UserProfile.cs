using PlayFab.ClientModels;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UserProfile : MonoBehaviour
{
    [SerializeField] ProfileData profileData;
    [SerializeField] List<PlayerLeaderboardEntry> leaderboardHighScore = new List<PlayerLeaderboardEntry>();
    public int highscore;

    public static UnityEvent<List<PlayerLeaderboardEntry>> OnLeaderboardHighScoreUpdated = new UnityEvent<List<PlayerLeaderboardEntry>>();

    public static UserProfile Instance;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        Instance = this;
    }

    private void OnEnable()
    {
        PlayfabAccountManager.OnSignInSuccess.AddListener(SignInSuccess);
        PlayfabAccountManager.OnUserDataRetrieved.AddListener(UserDataRetrieved);
        PlayfabAccountManager.OnLeaderboardRetrieved.AddListener(LeaderboardRetrieved);
        PlayfabAccountManager.OnStatisticRetrieved.AddListener(StatisticRetrieved);

        ScoreManager.OnFinaliseScore.AddListener(ShowDeathScreenHighscores);

        SceneLoader.OnStartScreenLoaded.AddListener(DestroyUserProfile);
    }

    private void OnDisable()
    {
        PlayfabAccountManager.OnSignInSuccess.RemoveListener(SignInSuccess);
        PlayfabAccountManager.OnUserDataRetrieved.RemoveListener(UserDataRetrieved);
        PlayfabAccountManager.OnLeaderboardRetrieved.RemoveListener(LeaderboardRetrieved);
        PlayfabAccountManager.OnStatisticRetrieved.RemoveListener(StatisticRetrieved);

        ScoreManager.OnFinaliseScore.RemoveListener(ShowDeathScreenHighscores);

        SceneLoader.OnStartScreenLoaded.AddListener(DestroyUserProfile);
    }

    private void SignInSuccess()
    {
        GetUserData();
        GetHighscoreStatistic();
        GetLeaderboardHighscore();
    }

    private void ShowDeathScreenHighscores(int finalScore)
    {
        if (finalScore > highscore)
        {
            highscore = finalScore;
            PlayfabAccountManager.Instance.SetStatistic("High Score", highscore);
        }
        GetLeaderboardHighscore();
    }

    IEnumerator WaitToShowNewLeaderboard()
    {
        yield return new WaitForEndOfFrame();
        GetLeaderboardHighscore();
    }

    [ContextMenu("Get User Data")]
    private void GetUserData()
    {
        PlayfabAccountManager.Instance.GetUserData("ProfileData");
    }

    private void UserDataRetrieved(string key, string value)
    {
        if (key == "ProfileData")
        {
            if (value != null && value.Length > 0)
            {
                profileData = JsonUtility.FromJson<ProfileData>(value);
            }
        }
    }

    [ContextMenu("Set User Data")]
    private void SetUserData()
    {
        PlayfabAccountManager.Instance.SetUserData("ProfileData", JsonUtility.ToJson(profileData));
    }

    private void GetHighscoreStatistic()
    {
        PlayfabAccountManager.Instance.GetStatistic("High Score");
    }

    [ContextMenu("Get Highscore Leaderboard")]
    private void GetLeaderboardHighscore()
    {
        PlayfabAccountManager.Instance.GetLeaderboard("High Score");
    }

    [ContextMenu("Increase Highscore")]
    public void IncreaseHighscore()
    {
        highscore += 1;
        PlayfabAccountManager.Instance.SetStatistic("High Score", highscore);
    }

    private void LeaderboardRetrieved(string statistic, List<PlayerLeaderboardEntry> leaderboard)
    {
        if (statistic == "High Score")
        {
            leaderboardHighScore = leaderboard;
            OnLeaderboardHighScoreUpdated.Invoke(leaderboardHighScore);
        }
    }

    private void StatisticRetrieved(string statistic, StatisticValue value)
    {
        if (statistic == "High Score")
        {
            highscore = value.Value;
        }
    }

    private void DestroyUserProfile()
    {
        if (Instance == this)
        {
            Destroy(this.gameObject);
        }
    }
}

[System.Serializable]
public class ProfileData
{
    public string playerName;
    public int highestScore;
}
