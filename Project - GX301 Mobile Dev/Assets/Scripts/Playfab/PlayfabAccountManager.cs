using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine.Events;
using System;

public class PlayfabAccountManager : MonoBehaviour
{
    public static PlayfabAccountManager Instance;

    public static UnityEvent OnSignInSuccess = new UnityEvent();
    public static UnityEvent<string> OnSignInFail = new UnityEvent<string>();
    public static UnityEvent<string, string> OnUserDataRetrieved = new UnityEvent<string, string>();

    public static UnityEvent<string, List<PlayerLeaderboardEntry>> OnLeaderboardRetrieved = new UnityEvent<string, List<PlayerLeaderboardEntry>>();
    public static UnityEvent<string, StatisticValue> OnStatisticRetrieved = new UnityEvent<string, StatisticValue>();

    public static string playfabID;

    private void Awake()
    {
        Instance = this;
    }

    //ACCOUNT

    public void CreateAccount (string emailAddress, string username, string password)
    {
        Debug.Log($"Creating account for {emailAddress}, {username}, {password}");
        PlayFabClientAPI.RegisterPlayFabUser(new RegisterPlayFabUserRequest()
            {
                Email = emailAddress,
                Password = password,
                Username = username,
                RequireBothUsernameAndEmail = true
            }, 
            response =>
            {
                Debug.Log($"Successful Account Creation {username}, {emailAddress}");
                SignIn(username, password);
            }, 
            error =>
            {
                Debug.Log($"Unuccessful Account Creation {username}, {emailAddress} \n {error.ErrorMessage}");
                OnSignInFail.Invoke(error.ErrorMessage);
            });
    }

    public void SignIn(string username, string password)
    {
        PlayFabClientAPI.LoginWithPlayFab(new LoginWithPlayFabRequest()
        {
            Username = username,
            Password = password
        },
        response =>
        {
            Debug.Log($"Successful Account Login: {username}");
            playfabID = response.PlayFabId;
            CheckDisplayName(username, () => OnSignInSuccess.Invoke());
        },
        error =>
        {
            Debug.Log($"Unsuccessful Account Login: {username} \n {error.ErrorMessage}");
            OnSignInFail.Invoke(error.ErrorMessage);
        });
    }

    //DISPLAY NAME

    private void CheckDisplayName(string username, UnityAction completeAction)
    {
        PlayFabClientAPI.GetAccountInfo(new GetAccountInfoRequest()
        {
            PlayFabId = playfabID
        },
        response =>
        {
            if (response.AccountInfo.TitleInfo.DisplayName == null || response.AccountInfo.TitleInfo.DisplayName.Length == 0)
            {
                PlayFabClientAPI.UpdateUserTitleDisplayName(new UpdateUserTitleDisplayNameRequest()
                {
                    DisplayName = username
                },
                response =>
                {
                    Debug.Log($"Display Name set to Username");
                    completeAction.Invoke();
                },
                error =>
                {
                    Debug.Log($"Display name could not be set to username \n {error.ErrorMessage}");
                });
            }
            else
            {
                completeAction.Invoke();
            }
        },
        error =>
        {
            Debug.Log($"Could not retrieve Account Info \n {error.ErrorMessage}");
        });
    }

    //USER DATA

    internal void GetUserData(string key)
    {
        PlayFabClientAPI.GetUserData(new GetUserDataRequest()
        {
            Keys = new List<string>()
            {
                key
            },
            PlayFabId = playfabID
        },
        response =>
        {
            Debug.Log($"User Data Got");
            if (response.Data.ContainsKey(key)) 
                OnUserDataRetrieved.Invoke(key, response.Data[key].Value);
            else 
                OnUserDataRetrieved.Invoke(key, null);
        },
        error =>
        {
            Debug.Log($"No Got user data \n {error.ErrorMessage}");
        });
    }

    internal void SetUserData(string key, string value)
    {
        PlayFabClientAPI.UpdateUserData(new UpdateUserDataRequest()
        {
            Data = new Dictionary<string, string>()
            {
                { key, value }
            }
        },
        response =>
        {
            Debug.Log($"Could not update userdata for {key}");
        },
        error =>
        {
            Debug.Log($"Updated userdata for {key}");
        });
    }

    //LEADERBOARDS AND STATS


    public void GetStatistic(string statistic)
    {
        PlayFabClientAPI.GetPlayerStatistics(new GetPlayerStatisticsRequest()
        {
            StatisticNames = new List<string>()
            {
                statistic
            }
        },
        response =>
        {
            if (response.Statistics.Count > 0)
            {
                Debug.Log($"Successfully got {statistic} | {response.Statistics[0]}");
                if (response.Statistics != null) OnStatisticRetrieved.Invoke(statistic, response.Statistics[0]);
            }
            else
            {
                Debug.Log($"{statistic} does not exist for user");
                SetStatistic(statistic, 0);
            }
        },
        error =>
        {
            Debug.Log($"Could not retrieve {statistic} \n {error.ErrorMessage}");
        });
    }

    public void SetStatistic(string statistic, int value)
    {
        PlayFabClientAPI.UpdatePlayerStatistics(new UpdatePlayerStatisticsRequest()
        {
            Statistics = new List<StatisticUpdate>()
            {
                new StatisticUpdate()
                {
                    StatisticName = statistic,
                    Value = value
                }
            }
        },
        response =>
        {
            Debug.Log($"{statistic} updated to {value} successfully");
        },
        error =>
        {
            Debug.Log($"{statistic} update not successful \n {error.ErrorMessage}");
        });
    }

    public void GetLeaderboard(string statistic)
    {
        PlayFabClientAPI.GetLeaderboard(new GetLeaderboardRequest()
        {
            StatisticName = statistic
        },
        response =>
        {
            if (response.Leaderboard.Count > 0) Debug.Log($"Successfully got {statistic} leaderboard | 0.{response.Leaderboard[0].DisplayName} {response.Leaderboard[0].StatValue}");
            OnLeaderboardRetrieved.Invoke(statistic, response.Leaderboard);
        },
        error =>
        {
            Debug.Log($"Could not retrieve {statistic} leaderboard | {error.ErrorMessage}");
        });
    }

    internal void GetLeaderboardDelayed(string statistic)
    {
        StartCoroutine(CheckLeaderboardDelay(statistic));
    }

    IEnumerator CheckLeaderboardDelay(string statistic)
    {
        yield return new WaitForSeconds(3);
        GetLeaderboard(statistic);
    }
}
